# Civo Cluster Management

This project help you to 
- Create and manage a Civo Kubernetes cluster from **[Gitpod](https://www.gitpod.io/)**. It uses the **Civo CLI** to create the cluster on **[Civo](https://www.civo.com)**
- Setup the Kubernetes integration between the **Civo** cluster and **GitLab** thanks to **[Gitpod](https://www.gitpod.io/)** (again)

This project is an "adaptation" of https://docs.gitlab.com/ee/user/clusters/management_project_template.html

> the project is here https://gitlab.com/gitlab-org/project-templates/cluster-management

## Prerequisites

### Accounts

- Create an account on **GitLab.com**
- Create an account on **[Civo](https://www.civo.com)**
- Create an account on **[Gitpod](https://www.gitpod.io)**

### On Gitpod

- Set the `CIVO_API_KEY` environment variable in the GitPod's settings of your GitPod account

### On GitLab

- Create a group (or use an existing group)
- Import this project inside this group
- 🖐️ Set the project to **private** if you plan to commit some secrets or credentials (btw, it's a very bad idea)

**Then, open the project with Gitpod**:

- Update the `.env` file of this project with the appropriate values (Civo settings):

  ```bash
  CLUSTER_NAME="panda"
  CLUSTER_SIZE="g3.k3s.large"
  CLUSTER_NODES=1
  CLUSTER_REGION=NYC1
  KUBECONFIG=./config/k3s.yaml
  ```

## Integration preamble

To create the Kubernetes Civo cluster and get the config file to connect to the cluster with `kubectl`, run the `01-create-cluster.sh` script. This script will:

- Create the Kubernetes cluster on Civo
- Get the KUBECONFIG file

To get the necessary data to setup the GitLab integration, run the `02-integration-preamble.sh` script. This script will:

- Generate the certificate and the token of cluster
- Create the GitLab service account in the cluster

Once the cluster created, you can check the configuration with these commands:

```bash
kubectl version 
kubectl get nodes
kubectl get pods --all-namespaces
```

or run **K9s** from the Gitpod terminal:

```bash
k9s --all-namespaces
```

## Setup of Kubernetes integration between the Civo Cluster and the GitLab group

- Add Cluster Integration to the parent Group of this project using the certificate and the token
  - Go to the **Kubernetes** menu and choose the **Connect existing cluster** panel
  - You can fill the **Base domain** with a domain like `<IP.of.the.cluster>.nip.io`
  - Save the changes
- Define this current project as a cluster management project
  - Go to the **Advanced Settingf**
  - Choose this project as **cluster management project**
  - Save the changes
- Get the GitLab url and the runner token from the CI/CD settings of the group
  - You'll find the values by expanding the **Runners** section
- Update the values of `./applications/gitlab-runner/values.yaml` with the url and the token
  - `gitlabUrl: "https://gitlab.com/"`
  - `runnerRegistrationToken: "<YOUR-TOKEN>"`
- Update the content of `./helmfile.yaml` to install the needed applications (I uncommented the gitlab runner and prometheus lines)
- Run the CI of the cluster management project (commit and push your updates, then the pipeline will be triggered)

## Good to know

> - If you run `k9s --all-namespaces` in the Gitpod `k9s` terminal, you'll watch the creation of the GitLab runner (and other applications).
> - The `remove-cluster.sh` script allows you to destroy the Civo Kubernetes cluster
> - If you need to ket the cluster configuration file again, run the `get-kube-config.sh` script
